FROM alpine

RUN apk add --no-cache \
  bash       \
  curl       \
  ipvsadm    \
  iproute2   \
  keepalived \
  && rm /etc/keepalived/keepalived.conf

COPY keepalived /etc/keepalived/
COPY docker-entrypoint.sh /entrypoint.sh
RUN chmod +x  /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]