#!/bin/bash

#
# set localtime
ln -sf /usr/share/zoneinfo/$LOCALTIME /etc/localtime

#replace_vars
function replace_vars() {
  eval "cat <<EOF
  $(<$2)
EOF
  " > $1
}

replace_vars '/etc/keepalived/keepalived.conf' '/etc/keepalived/keepalived.tmpl.conf'

# Run
/usr/sbin/keepalived -P -d -D -S 7 -f /etc/keepalived/keepalived.conf --dont-fork --log-console
