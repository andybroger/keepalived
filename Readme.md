# keepalived for arm devices

add `net.ipv4.ip_nonlocal_bind=1` to your /etc/sysctl.conf and reboot

## USAGE

Make sure to change the Priority on the nodes. (highest = master, max 255)

```shell
docker run -d --name keepalived --restart=always --net=host --cap-add=NET_ADMIN \
    -e PRIORITY='100' \
    -e INTERFACE='wlan0' \
    -e VIP='10.1.1.10' \
    -e PEER1='10.1.1.11' \
    -e PEER2='10.1.1.12' \
    -e PEER3='10.1.1.13' \
    keepalived
```
